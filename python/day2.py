with open('../inputs/input2.txt', 'r') as file:
  l = [x.strip().split() for x in file]

# Part 1
pos_x = 0
pos_y = 0

for s in l:
  direction = s[0]
  length = int(s[1])
  if direction == 'forward':
    pos_x += length
  elif direction == 'down':
    pos_y += length
  else:
    pos_y -= length

print(f'Day 2 - part 1 : {pos_x*pos_y}')

# Part 2
pos_x = 0
pos_y = 0
aim = 0

for s in l:
  direction = s[0]
  length = int(s[1])
  if direction == 'up':
    aim -= length
  elif direction == 'down':
    aim += length
  else:
    pos_x += length
    pos_y += length*aim

print(f'Day 2 - part 2 : {pos_x*pos_y}')
