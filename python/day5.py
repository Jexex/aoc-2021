input = '../inputs/input5.txt'
# input = '../inputs/input5_test.txt'

with open(input, 'r') as file:
  l = [x.strip() for x in file]

size = 1000
# size = 10 # Uncomment for testing

points = [([int(k) for k in y[0].split(',')], [int(k) for k in y[2].split(',')]) for y in [[s for s in x.split()] for x in l]]
grid1 = [[0 for _ in range(size)] for _ in range(size)]
grid2 = [[0 for _ in range(size)] for _ in range(size)]

def print_grid(g):
  for x in g:
    for s in x:
      print(f' {"." if s == 0 else s} ', end='')
    print('')

# Part 1

def fill_grid_part1(point):
  p1 = point[0]
  p2 = point[1]
  dx = p1[0] - p2[0]
  dy = p1[1] - p2[1]
  if dy == 0:
    m = min(p1[0],p2[0])
    n = max(p1[0],p2[0])
    for s in range(m,n+1):
      grid1[p1[1]][s] += 1
  elif dx == 0:
    m = min(p1[1],p2[1])
    n = max(p1[1],p2[1])
    for s in range(m,n+1):
      grid1[s][p1[0]] += 1

def get_result(g):
  count = 0
  for x in g:
    for y in x:
      if y > 1: count += 1
  return count

for point in points:
  fill_grid_part1(point)

# print_grid(grid1)
print(f'Day 5 - Part 1 : {get_result(grid1)}')

# Part 2

def fill_grid_part2(point):
  p1 = point[0]
  p2 = point[1]
  dx = p1[0] - p2[0]
  dy = p1[1] - p2[1]
  # print(f'P1 : {p1}  P2 : {p2}')
  if dy == 0:
    m = min(p1[0],p2[0])
    n = max(p1[0],p2[0])
    # print(f'It is a ROW')
    for s in range(m,n+1):
      grid2[p1[1]][s] += 1
  elif dx == 0:
    m = min(p1[1],p2[1])
    n = max(p1[1],p2[1])
    # print(f'It is a COLUMN')
    for s in range(m,n+1):
      grid2[s][p1[0]] += 1
  elif dx == dy:
    p = p1 if p1[0] < p2[0] else p2
    # print(f'It is a diagonal going DOWN  P : {p}')
    for x in range(abs(dx)+1):
      grid2[p[1]+x][p[0]+x] += 1
  elif dx == -dy:
    p = p1 if p1[1] > p2[1] else p2
    # print(f'It is a diagonal going UP  P : {p}')
    for x in range(abs(dx)+1):
      grid2[p[1]-x][p[0]+x] += 1

for point in points:
  fill_grid_part2(point)

# print_grid(grid2)
print(f'Day 5 - Part 2 : {get_result(grid2)}')
