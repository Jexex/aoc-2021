with open('../inputs/input4.txt', 'r') as file:
  l = [x.strip() for x in file]

# Override input for tests
# l = ["7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1","", "14 21 17 24 4", "10 16 15 9 19", "18 8 23 26 20", "22 11 13 6 5", "2 0 12 3 7",""]

numbers = [int(x) for x in l[0].split(',')]

cards = []
card = []

# Calculate the score of a winning board
def score(numbers, card):
  unmarked = sum([x for x in card if x not in numbers])
  last_number = numbers[-1]
  return unmarked*last_number
  # print(f'Unmarked total : {unmarked}\nLast number : {last_number}\nTotal score : {s}')

# Checks if a specific card wins with specific numbers
def check_score(numbers, card):
  # print(f'Numbers : {numbers}\nCard : {card}')
  for x in range(5):
    row = card[x*5:x*5+5]
    col = [card[y] for y in range(len(card)) if (y-x)%5==0]
    # print(f'Row : {row}\nColumn : {col}')
    if all(item in numbers for item in row):
      # print(f'Winner. Row : {row}')
      return score(numbers, card)
    elif all(item in numbers for item in col):
      # print(f'Winner. Column : {col}')
      return score(numbers, card)
  return 0

# Arrange all cards data
for x in range(2,len(l)):
  if len(l[x]) == 0:
    cards.append(card)
    card = []
    continue
  card += [(int(a)) for a in l[x].split()]

# Part 1

# Check for a winner!
def part1_answer():
  for x in range(5, len(numbers)):
    currentNumbers = numbers[0:x]
    for card in cards:
      s = check_score(currentNumbers, card)
      if s != 0:
        return s

print(f'Day 4 - part 1 : {part1_answer()}')

# Part 2

def get_winners(numbers, cards):
  winners = []
  for card in cards:
    s = check_score(numbers, card)
    if s != 0:
      winners.append((card,s))
  return winners

def part2_answer():
  currentCards = cards
  for x in range(5, len(numbers)):
    currentNumbers = numbers[0:x]
    winners = get_winners(currentNumbers,currentCards)
    for y in winners:
      currentCards.remove(y[0])
      last_win = y[0]
      last_score = y[1]
  return last_score

print(f'Day 4 - part 2 : {part2_answer()}')
