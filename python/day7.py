import statistics

input = '../inputs/input7.txt'
# input = '../inputs/input7_test.txt'

with open(input, 'r') as file:
  l = [x.strip() for x in file]

positions = [int(x) for x in l[0].split(',')]
median = statistics.median(positions)

def cost(position):
  return sum(map(lambda x: abs(x-position), positions))

best = (median, cost(median))

print(f'Day 7 - Part 1 : {best[1]}')

# Part 2

def cost_p2_position(currentPosition, targetPosition):
  move = abs(currentPosition-targetPosition)
  return 0.5*move*(move+1)

def cost_p2(position):
  return sum(map(lambda x: cost_p2_position(x, position), positions))

best = (median, cost_p2(median))

for x in range(min(positions), max(positions)):
  fuel = cost_p2(x)
  if fuel < best[1]:
    best = (x, fuel)

print(f'Day 7 - Part 2 : {best[1]}')
