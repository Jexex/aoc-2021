with open('../inputs/input3.txt', 'r') as file:
  l = [x.strip() for x in file]

# Override values with test values.
# Part 1 : Expecting 198
# Part 2 : Expecting 230
# l = ['00100','11110','10110','10111','10101','01111','00111','11100','10000','11001','00010','01010']

# Part 1
gamma = ''
epsilon = ''

for i in range(len(l[0])):
  a = ''.join([x[i] for x in l])
  gamma += ('0','1')[a.count('0')>a.count('1')]
  epsilon += ('0','1')[a.count('0')<a.count('1')]

print(f'Day 3 - part 1 : {int(gamma,2)*int(epsilon,2)}')

# Part 2
def get_char(s, most):
  count0 = s.count('0')
  count1 = s.count('1')

  if count0 == count1:
    return '1' if most else '0'

  mostChar = ('0','1')[count0 < count1]
  lessChar = ('0','1')[count0 > count1]

  return mostChar if most else lessChar

def get_numbers(numbers, position, most):
  s = ''.join(s[position] for s in numbers)
  res = list(filter(lambda z:z[position]==get_char(s,most), numbers))

  return res[0] if len(res) == 1 else get_numbers(res, position+1, most)


oxygen = get_numbers(l, 0, True)
co2 = get_numbers(l, 0, False)

print(f'Day 3 - part 2 : {int(oxygen,2)*int(co2,2)}')
