with open('../inputs/input1.txt', 'r') as file:
  l = [int(x) for x in file]

# Part 1
counter = 0

print(len(l))

for x in range(1,len(l)):
  if l[x] > l[x-1]:
    counter += 1

print(f'Day 1 - part 1 : {counter}')

# Part 2
counter = 0

for x in range(0,len(l)-2):
  if sum(l[x+1:x+4]) > sum(l[x:x+3]):
    counter += 1

print(f'Day 1 - part 2 : {counter}')
