input = '../inputs/input8.txt'
# input = '../inputs/input8_test1.txt'
# input = '../inputs/input8_test2.txt'

with open(input, 'r') as file:
  l = [x.strip() for x in file]

data = [(y.split(),z.split()) for y,z in [x.split('|') for x in l]]

# Part 1

count = 0

for x in data:
  signals = x[0]
  pattern = x[1]
  count += len([1 for n in pattern if len(n) in [2,3,4,7]])

print(f'Day 8 - Part 1 : {count}')

def decrypt_signals(s):
  answers = {
    '0' : None,
    '1' : None,
    '2' : None,
    '3' : None,
    '4' : None,
    '5' : None,
    '6' : None,
    '7' : None,
    '8' : None,
    '9' : None,
  }
  s2 = [list(sorted(x)) for x in s if len(x) == 2][0]
  s3 = [list(sorted(x)) for x in s if len(x) == 3][0]
  s4 = [list(sorted(x)) for x in s if len(x) == 4][0]
  s5 = [list(sorted(x)) for x in s if len(x) == 5]
  s6 = [list(sorted(x)) for x in s if len(x) == 6]
  s7 = [list(sorted(x)) for x in s if len(x) == 7][0]

  # Easy ones
  answers['1'] = s2
  answers['4'] = s4
  answers['7'] = s3
  answers['8'] = s7

  answers['3'] = [x for x in s5 if set(s2).issubset(x)][0]
  s5.remove(answers['3'])

  answers['2'] = [x for x in s5 if len(set(s4).intersection(x)) == 2][0]
  s5.remove(answers['2'])

  answers['5'] = s5[0]

  answers['9'] = [x for x in s6 if set(answers['3']).issubset(x)][0]
  s6.remove(answers['9'])

  answers['6'] = [x for x in s6 if set(answers['5']).issubset(x)][0]
  s6.remove(answers['6'])

  answers['0'] = s6[0]

  return answers

total = 0
for x in data:
  signals = x[0]
  table = decrypt_signals(signals)
  code = ''
  for s in x[1]:
    code += list(table.keys())[list(table.values()).index(sorted(s))]
  total += int(code)

print(f'Day 8 - Part 2 : {total}')
