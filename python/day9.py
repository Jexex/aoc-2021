input = '../inputs/input9.txt'
# input = '../inputs/input9_test.txt'

with open(input, 'r') as file:
  l = [[9]+[int(y) for y in x.strip()]+[9] for x in file]

# Pad list with 9s
l.insert(0, [9 for _ in range(len(l[1]))])
l.append([9 for _ in range(len(l[1]))])

total = 0

for row in range(1,len(l)-1):
  for x in range(1,len(l[0])-1):
    val = l[row][x]
    if (l[row-1][x] > val and
        l[row+1][x] > val and
        l[row][x-1] > val and
        l[row][x+1] > val):
      total += val + 1

print(f'Day 9 - Part 1 : {total}')

def explore(row, x):
  val = l[row][x]
  if val == 9 or val == '-':
    return 0
  count = 1
  l[row][x] = '-'
  count += explore(row-1,x)
  count += explore(row+1,x)
  count += explore(row,x+1)
  count += explore(row,x-1)
  return count

total = 0
bassins = []

for row in range(1,len(l)-1):
  for x in range(1,len(l[0])-1):
    y = explore(row,x)
    if y != 0:
      bassins.append(y)

bassins.sort()

print(f'Day 9 - Part 2 : {bassins[-1]*bassins[-2]*bassins[-3]}')
