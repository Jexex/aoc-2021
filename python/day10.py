import statistics

input = '../inputs/input10.txt'
# input = '../inputs/input10_test.txt'

with open(input, 'r') as file:
  l = [x.strip() for x in file]

symbol = {
  '<' : '>',
  '(' : ')',
  '[' : ']',
  '{' : '}'
}

# Part 1

points = {
  '>' : 25137,
  ')' : 3,
  ']' : 57,
  '}' : 1197
}

total = 0
for x in l:
  chunks = []
  for c in x:
    if c in '<[{(':
      chunks.append(c)
    else:
      last = chunks.pop()
      if c != symbol[last]:
        # Corrupted line
        total += points[c]
        break

print(f'Day 10 - Part 1 : {total}')

points = {
  '<' : 4,
  '(' : 1,
  '[' : 2,
  '{' : 3
}

total = []
for x in l:
  chunks = []
  subtotal = 0
  count = True
  for c in x:
    if c in '<[{(':
      chunks.append(c)
    else:
      last = chunks.pop()
      if c != symbol[last]:
        # Corrupted line (ignore that and don't count it)
        count = False
        break
  if count:
    for s in reversed(chunks):
      subtotal = subtotal * 5
      subtotal += points[s]
    total.append(subtotal)

print(f'Day 10 - Part 2 : {statistics.median(total)}')
