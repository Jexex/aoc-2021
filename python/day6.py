input = '../inputs/input6.txt'
# input = '../inputs/input6_test.txt'

with open(input, 'r') as file:
  l = [x.strip() for x in file]

fishes = [int(x) for x in l[0].split(',')]

# Part 1

def get_fishes(currentFishes):
  newFishes = []
  for fish in currentFishes:
    if fish == 0:
      newFishes.append(6)
      newFishes.append(8)
    else:
      newFishes.append(fish-1)
  return newFishes

fishes_p1 = fishes.copy()

for x in range(0,80):
  fishes_p1 = get_fishes(fishes_p1)
print(f'Day 6 - Part 1 : {len(fishes_p1)}')

# Part 2

reproduction = dict()

# Fill dict
for s in range(9):
  f = [s]
  for x in range(0,128):
    f = get_fishes(f)
  reproduction[s] = f

def get_fishes_p2(currentFishes):
  newFishes = []
  for fish in currentFishes:
    newFishes.extend(reproduction[fish])
  return newFishes

def get_fishes_p2_total(currentFishes):
  total = 0
  for fish in currentFishes:
    total += len(reproduction[fish])
  return total

fishes_p2 = get_fishes_p2(fishes)

print(f'Day 6 - Part 2 : {get_fishes_p2_total(fishes_p2)}')
