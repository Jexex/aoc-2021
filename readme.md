# Advent of Code 2021

[Day 1](day1.py) :star: :star: [Advent of Code](https://adventofcode.com/2021/day/1) :christmas_tree:

[Day 2](day2.py) :star: :star: [Advent of Code](https://adventofcode.com/2021/day/2) :christmas_tree:

[Day 3](day3.py) :star: :star: [Advent of Code](https://adventofcode.com/2021/day/3) :christmas_tree:

[Day 4](day4.py) :star: :star: [Advent of Code](https://adventofcode.com/2021/day/4) :christmas_tree:

[Day 5](day5.py) :star: :star: [Advent of Code](https://adventofcode.com/2021/day/5) :christmas_tree:

[Day 6](day6.py) :star: :star: [Advent of Code](https://adventofcode.com/2021/day/6) :christmas_tree:

[Day 7](day7.py) :star: :star: [Advent of Code](https://adventofcode.com/2021/day/7) :christmas_tree:

[Day 8](day8.py) :star: :star: [Advent of Code](https://adventofcode.com/2021/day/8) :christmas_tree:

[Day 9](day9.py) :star: :star: [Advent of Code](https://adventofcode.com/2021/day/9) :christmas_tree:

[Day 10](day10.py) :star: :star: [Advent of Code](https://adventofcode.com/2021/day/10) :christmas_tree: 
